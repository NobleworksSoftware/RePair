package com.nobleworks_software.repair

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.jetbrains.spek.api.lifecycle.CachingMode
import org.jetbrains.spek.subject.SubjectSpek

internal object PairBufferSpek : SubjectSpek<PairBuffer>(
{
    val content = "abcdebcdf"

    given("PairBuffer with '$content'")
    {
        subject(CachingMode.SCOPE) { PairBuffer(content) }

        val firstReplacement = 0xF0000
        val secondReplacement = firstReplacement + 1
        val firstIndex = content.indexOf("bcd")
        val secondIndex = content.indexOf("bcd", firstIndex)

        on("replacing bc with symbol $firstReplacement")
        {
            subject.replacePair(firstIndex, firstReplacement)

            arrayOf(firstIndex, secondIndex).forEach()
            {
                it("should return $firstReplacement for symbolAt($it)")
                {
                    assertThat(subject.symbolAt(it), equalTo(firstReplacement))
                }
            }
        }

        on("then replacing [bc]d with symbol $secondReplacement")
        {
            subject.replacePair(firstIndex, secondReplacement)

            arrayOf(firstIndex, secondIndex).forEach()
            {
                it("should return $secondReplacement for symbolAt($it)")
                {
                    assertThat(subject.symbolAt(it), equalTo(secondReplacement))
                }
            }
        }
    }
})