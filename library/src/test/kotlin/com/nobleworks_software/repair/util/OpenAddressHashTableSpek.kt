package com.nobleworks_software.repair.util

import com.natpryce.hamkrest.*
import com.natpryce.hamkrest.assertion.assertThat
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.jetbrains.spek.subject.SubjectSpek

internal class TestHashTable(val size: Int) : OpenAddressHashTable<String>(size), SelfDescribing
{
    override val String.hash get() = toInt()

    fun contains(expected: Iterable<String?>)
        = expected.filterIndexed { index, s -> s != this[index] }.isEmpty()

    fun find(value: Int) = this.findValue(value) { it.equals(value.toString()) }

    override val description: String
        get() = (0 until size).map { this[it] }.joinToString(prefix = "TestHashTable[", postfix = "]")
}

internal fun hasContent(expected: Iterable<String?>) = Matcher(TestHashTable::contains, expected)

data class DeleteTestCase(val content: Iterable<String>,
                          val itemsToDelete: Iterable<String>,
                          val expectedContent: List<String?>)

internal object OpenAddressHashTableSpek : SubjectSpek<TestHashTable>(
{
    describe("TestHashTable of size 5")
    {
        subject { TestHashTable(5) }

        it("should default to empty")
        {
            assertThat(subject, hasContent(List(5) {null}))
        }

        listOf(
                listOf("0") to listOf("0", null, null, null, null),
                listOf("1") to listOf(null, "1", null, null, null),
                listOf("2") to listOf(null, null, "2", null, null),
                listOf("3") to listOf(null, null, null, "3", null),
                listOf("4") to listOf(null, null, null, null, "4"),
                listOf("5") to listOf("5", null, null, null, null),
                listOf("6") to listOf(null, "6", null, null, null),
                listOf("7") to listOf(null, null, "7", null, null),
                listOf("8") to listOf(null, null, null, "8", null),
                listOf("9") to listOf(null, null, null, null, "9"),
                listOf("10") to listOf("10", null, null, null, null),
                listOf("1", "6") to listOf(null, "1", "6", null, null),
                listOf("6", "1") to listOf(null, "6", "1", null, null),
                listOf("6", "1", "2") to listOf(null, "6", "1", "2", null),
                listOf("2", "1", "6") to listOf(null, "1", "2", "6", null),
                listOf("1", "2", "6") to listOf(null, "1", "2", "6", null),
                listOf("6", "2", "1") to listOf(null, "6", "2", "1", null)
        )
        .forEach()
        {(valuesToInsert, expectedContent) ->
            on("inserting $valuesToInsert")
            {
                valuesToInsert.forEach { subject.insert(it) }

                it("should contain $expectedContent")
                {
                    assertThat(subject, hasContent(expectedContent))
                }

                valuesToInsert.forEach()
                {
                    it("should find $it")
                    {
                        assertThat(subject.find(it.toInt()), present(sameInstance(it)))
                    }
                }
            }
        }

        listOf(
                DeleteTestCase(listOf("1", "2", "6"), listOf("1"), listOf(null, "6", "2", null, null)),
                DeleteTestCase(listOf("6", "2", "1"), listOf("6"), listOf(null, "1", "2", null, null)),
                DeleteTestCase(listOf("6", "1", "2"), listOf("6"), listOf(null, "1", "2", null, null)),
                DeleteTestCase(listOf("1", "11", "6"), listOf("1"), listOf(null, "11", "6", null, null))
        )
        .forEach()
        { (content, itemsToDelete, expectedResult) ->
            on("inserting $content then deleting $itemsToDelete")
            {
                subject
                    .apply { content.forEach { insert(it) } }
                    .apply { itemsToDelete.forEach { remove(it) } }


                it("should contain $expectedResult")
                {
                    assertThat(subject, hasContent(expectedResult))
                }

                content.minus(itemsToDelete).forEach()
                {
                    it("should find $it")
                    {
                        assertThat(subject.find(it.toInt()), present(sameInstance(it)))
                    }
                }

                itemsToDelete.forEach()
                {
                    it("should not find $it")
                    {
                        assertThat(subject.find(it.toInt()), absent())
                    }
                }
            }
        }
    }
})
