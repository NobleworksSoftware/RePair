package com.nobleworks_software.repair

import java.io.FileReader
import java.util.*

object TestApp
{
    private var dictionary: MutableMap<Int, IntArray> = HashMap()
    private var symbol = 0

    val replacementStart = 0xF0000

    private val Int.output
        get() = if (this >= replacementStart) "[${this - replacementStart}]" else toChar().toString()

    private fun decode(symbol: Int)
    {
        val decode = dictionary[symbol]

        if (decode != null)
        {
            decode(decode[0])
            decode(decode[1])
        }
        else
        {
            print(symbol.toChar())
        }
    }

    private fun add(first: Int, second: Int)
        = (replacementStart + symbol++)
            .also { dictionary.put(it, intArrayOf(first, second)) }
            .also { println("${it.output}-> '${first.output}${second.output}'") }

    @JvmStatic fun main(args: Array<String>)
    {
        val content = FileReader(args[0]).readLines().joinToString("\n")

        val output = RePair.compress(content, this::add)

        println(content.length)
        println(output.codePoints().count())
    }
}
