package com.nobleworks_software.repair

import java.util.*

@Suppress("RemoveEmptyParenthesesFromLambdaCall")
internal class PriorityQueue(size: Int) : Iterable<PriorityQueueEntry>
{
    /**
     * This is the array of references to the linked list of entries for a
     * particular count. The entry at index i refers to the list with a count
     * equal to i - 2, because we don't care about counts less than 2.
     */
    private val entries: Array<PriorityQueueEntry?> = arrayOfNulls(size)

    /**
     * This is the index to the highest non-empty bucket.
     */
    private val maxIndex = object : Iterator<PriorityQueueEntry>
    {
        private var max: Int = -1

        operator fun plusAssign(index: Int)
        {
            max = maxOf(max, index)
        }

        val bucket: Int?
            get()
            {
                while (max >= 0 && entries[max] == null)
                {
                    max--
                }

                return max.takeUnless { it < 0 }
            }

        override operator fun hasNext() = bucket != null

        override operator fun next(): PriorityQueueEntry
                = bucket?.let { entries[it]!!.removeFrom(it) }
                ?: throw NoSuchElementException()
    }

    /**
     * Calculates the index for the bucket for entries with the given count.
     *
     * @return The index for a bucket of the given count. The value returned
     *         will be min(count - 2, index for largest bucket). If the value
     *         returned is less than zero that means that the count is too small
     *         to warrant an entry in the queue.
     */
    private val PriorityQueueEntry.queueIndex: Int?
        // Subtract 2 so that a count of 2 goes into the first bucket
        // (bucket 0). We don't care about counts less than 2, so they
        // will have a negative index to indicate they don't belong in
        // a bucket.
        // If the count is higher than the last bucket, put it into the
        // last bucket.
        get() = minOf(count - 2, entries.size - 1).takeUnless { it < 0 }

    /**
     * Add the given entry to the correct priority queue bucket\
     *
     * @param entry The entry to add to the queue.
     * @returns true if the entry was added.
     */
    fun addEntry(entry: PriorityQueueEntry): Boolean
        = entry.queueIndex?.also()
        {
            log(QUEUE_DEBUG) {"Adding $entry to priority queue at index $it"}

            maxIndex += it

            entry.flag = false
            entries[it] = entry.insertIntoList(entries[it])
        } != null

    private fun PriorityQueueEntry.removeFrom(index: Int): PriorityQueueEntry
        = this.apply()
        {
            flag = true
            entries[index] = removeFrom(entries[index])
        }

    /**
     * Remove the given entry from its bucket, usually in preparation for
     * changing its count after which it will get re-added.
     *
     * @param entry Entry to remove
     */
    fun removeEntry(entry: PriorityQueueEntry)
        = entry.queueIndex
            ?.log(QUEUE_DEBUG) {"removing $entry from bucket $this"}
            ?.let { entry.removeFrom(it) }
            ?: throw IllegalStateException("Trying to remove an item from the queue with a count < 2")

    override operator fun iterator() = maxIndex
}