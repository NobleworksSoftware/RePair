package com.nobleworks_software.repair.util

@Suppress("RemoveEmptyParenthesesFromLambdaCall")
abstract class DoublyLinkedListItem<T : DoublyLinkedListItem<T>> : Comparable<T>
{
    /**
     * Cast this instance to subclass type
     */
    protected abstract val self: T

    /**
     * The next entry in the doubly-linked list.
     */
    private var nextEntry: T? = null

    /**
     * The previous entry in the doubly-linked list.
     */
    private var prevEntry: T? = null

    private fun linkBetween(previous: T?, next: T?)
    {
        assert(nextEntry == null)
        assert(prevEntry == null)

        next?.let()
        {
            assert(it.prevEntry === previous)
            nextEntry = it
            it.prevEntry = self
        }

        previous?.let()
        {
            assert(it.nextEntry === next)
            prevEntry = it
            it.nextEntry = self
        }
    }

    private fun unlink(): T?
    {
        // If there is an entry after this one
        nextEntry?.let()
        {
            assert(it.prevEntry === this)

            // Point it to the entry before the one being removed
            it.prevEntry = prevEntry
        }

        // If there is an entry before this one
        prevEntry?.let()
        {
            assert(it.nextEntry === this)

            // Point it to the entry after the one being removed
            it.nextEntry = nextEntry
        }

        // Remove the links from the entry being removed
        prevEntry = null
        return nextEntry.also { nextEntry = null }
    }

    /**
     * Insert this entry into the given doubly-linked list in sorted
     * order.
     *
     * @param list The head of the existing list or null if the list is empty.
     * @return The head of the new list, which is either the existing
     * list or this entry if the the entry is inserted at the head of the list.
    */
    fun insertIntoList(list: T?): T = insertIntoList(list, list, null)

    private tailrec fun insertIntoList(list: T?, next: T? = list, prev: T? = null): T
        = if(next != null && this > next)
        {
            insertIntoList(list, next.nextEntry, next)
        }
        else
        {
            linkBetween(prev, next)

            if (prev != null) list!! else self
        }

    /**
     * Insert the entry as the new head of the given list.
     *
     * @param list The current head of the existing list or null
     * if the existing list is empty.
     * @return This entry which is the new head of the list.
    */
    fun addBefore(list: T?): T = self.apply { linkBetween(null, list) }

    /**
     * Remove this entry from the list whose first entry is the given entry.
     * Takes care of adjusting the doubly-linked pointers.
     *
     * @param list The list item in the list that this entry should be removed
     * from. This may not be null.
     * @return The new list item for the list, which will be the current list
     *         item unless the list item is the one being removed.
     */
    fun removeFrom(list: T?): T?
        = when(list)
        {
            null -> throw IllegalStateException("list is empty")
            this -> unlink()
            else -> list.also { unlink() }
        }
}
