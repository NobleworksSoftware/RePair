package com.nobleworks_software.repair.util

import com.nobleworks_software.repair.HASHTABLE_VERBOSE
import com.nobleworks_software.repair.log
import java.util.*

internal abstract class OpenAddressHashTable<T>(size: Int)
{
    /**
     * This is the data for the hash table. It is an array of references to
     * priority queue entries.
     */
    @Suppress("UNCHECKED_CAST")
    private val table: Array<T?> = arrayOfNulls<Any>(size) as Array<T?>

    // Protected instead of private to allow subclasses to call inline function findValue
    protected operator fun get(index: Int) : T?
    {
        lastHashIndex = index % table.size
        return table[lastHashIndex]
    }

    /**
     * This is the index of the last findPair operation. It is used as an
     * optimization for the delete operation to avoid doing a full lookup when
     * deleting an entry that was just found.
     */
    private var lastHashIndex = 0

    /**
     * The number of free slots available in the hash table. We always make sure
     * to leave at least one free slot so there is at least one empty cell to end
     * any search on.
     */
    private var freeSlots: Int = table.size - 1

    /**
     * Find the value satisfying the given predicate starting at the
     * the given hash value. If there is no value satisfying the
     * predicate null is returned.
     *
     * @param hash The hashCode a value satisfying the predicate would have
     * @return The value satisfying the predicate or null if none is found.
     */
    protected inline fun findValue(hash: Int, predicate:(T) -> Boolean): T?
    {
        var value = this[hash].log(HASHTABLE_VERBOSE) {"Searching examining $this at $lastHashIndex"}

        while(value != null && !predicate(value))
        {
            value = this[lastHashIndex + 1].log(HASHTABLE_VERBOSE) {"Searching examining $this at $lastHashIndex"}
        }

        return value.log(HASHTABLE_VERBOSE) {"Search result $this at $lastHashIndex"}
    }

    protected abstract val T.hash: Int

    /**
     * Find the hash table index for the given value. If the value is
     * already in the hash table it returns the index where the reference to it
     * it is stored. If the value is not in the table it is the index where the
     * value should be inserted.
     *
     * @param valueToFind The entry to findPair in the table.
     * @return The index where the reference to the value is currently stored or
     *         the index where the entry should be inserted.
     * @throws NullPointerException If valueToFind is null.
     */
    private tailrec fun lookup(valueToFind: T, index: Int = valueToFind.hash): Int
    {
        val entry = this[index]

        return if(entry == null || entry === valueToFind) lastHashIndex; else lookup(valueToFind, index + 1)
    }

    /**
     * Insert a value into the HashTable.
     *
     * @param value The value to insert.
     * @throws IllegalStateException If the HashTable is full.
     */
    open fun insert(value: T)
    {
        if(table[lookup(value)] !== value)
        {
            if(freeSlots > 0)
            {
                table[lastHashIndex] = value.log(HASHTABLE_VERBOSE) {"Storing $this at $lastHashIndex in the hash table"}
                freeSlots--
            }
            else
            {
                throw IllegalStateException("No room left in hash table")
            }
        }
    }

    private tailrec fun rehash(index: Int)
    {
        val e = this[index]
        val oldIndex = lastHashIndex

        if (e != null)
        {
            table[oldIndex] = null
            table[lookup(e)] = e

            e.log(HASHTABLE_VERBOSE) {"Rehashed $this from $oldIndex to $lastHashIndex"}

            rehash(oldIndex + 1)
        }
        else
        {
            log(HASHTABLE_VERBOSE) {"Ended rehashing at $index"}
        }
    }

    /**
     * Remove a value from the HashTable.
     *
     * @param value The value to remove.
     * @throws NoSuchElementException If the value is not in the HashTable.
     */
    open fun remove(value: T)
    {
        // Assume the optimistic case that it is the last one we found.
        if (table[lastHashIndex] === value || table[lookup(value)] === value)
        {
            value.log(HASHTABLE_VERBOSE) {"Removing $this from hashTable $lastHashIndex"}

            // Null out the deleted value
            table[lastHashIndex] = null
            freeSlots++

            // Have to rehash following entries
            rehash(lastHashIndex + 1)
        }
        else
        {
            throw NoSuchElementException()
        }
    }
}