package com.nobleworks_software.repair

@Suppress("RemoveEmptyParenthesesFromLambdaCall")
class RePair private constructor(input: CharSequence)
{
    init
    {
        input.log(REPAIR_INFO) { "input has $length characters" }
    }

    companion object
    {
        /**
         * This is the symbol used to separate strings.
         */
        val separatorSymbol: Int = '\uFFFF'.toInt()

        fun compress(input: CharSequence, dictionary: Dictionary)
            = RePair(input).compress(dictionary)
    }

    private val workingList = object : Iterable<PriorityQueueEntry>
    {
        private var list: PriorityQueueEntry? = null

        fun add(entry: PriorityQueueEntry)
        {
            log(WORKING_LIST_DEBUG) {"Adding $entry to working list"}
            list = entry.addBefore(list)
        }

        fun remove(entry: PriorityQueueEntry)
                = list?.log(WORKING_LIST_DEBUG) {"Removing $entry from working list"}
                ?.let { list = entry.removeFrom(it) }
                ?: throw IllegalStateException("freeEntries is empty")

        override fun iterator(): Iterator<PriorityQueueEntry>
                = object : Iterator<PriorityQueueEntry>
        {
            override fun hasNext() = list != null
            override fun next() = list?.apply { remove(this) }
                    ?: throw NoSuchElementException()
        }
    }

    private val text = PairBuffer(input)
    private val hashTable = HashTable(text)
    private var queue = fillHashTableAndQueue()

    // Follow the chain and count the non-overlapping occurrences.
    tailrec fun countNonOverlappingOccurrences(firstIndex: Int, i: Int = firstIndex, count: Int = 1): Int
    {
        val next = text.nextNonOverlappingOccurrence(i)
        return when(next)
        {
            firstIndex -> count
            else -> countNonOverlappingOccurrences(firstIndex, next, count + 1)
        }
    }

    // This variable is for handling consecutive pair replacement
    // which causes repeated occurrences of the new symbol. It is
    // the index of the right symbol of the first occurrence of
    // a pair consisting of only the new symbol or -1 if no such
    // occurrence has been found.
    private val AA_Flag = object
    {
        var AA_index = -1

        fun reset()
        {
            AA_index = -1
        }

        fun isNotFirstAA(xIndex: Int, aIndex: Int): Boolean
        {
            if (AA_index < 0)
            {
                // If we haven't had one yet, record this as the first.
                AA_index = aIndex
            }

            // This is not the first occurrence. If this one does
            // not overlap the first one, then its OK to create an
            // entry for the repeated new symbol (i.e. AA)
            return AA_index < xIndex
        }

        fun isNonOverlappingAA(xIndex: Int, aIndex: Int)
            = if (AA_index != xIndex)
            {
                AA_index = aIndex

                true
            }
            else
            {
                // This is an overlapping instance of AA that
                // does not change the count
                false
            }
    }

    // These flags handle the special case when replacing ab in
    // a context like baby or xaba. When replacing ab in the
    // context xaby you set flags on xa and by entries to track
    // whether we have seen the the xa and by pairs before and if
    // so we know that we will have 2 instances of xA or Ay.
    // The problem is that when x == b or y == a. If replacing
    // ab in baby the ba flag is used as an indication for bA
    // and replacing ab in xaba, the ba flag is also used for
    // indication of Aa. Since the flag on ba can't
    // differentiate the two cases to tell if the flag was set
    // for bA or for Aa we ignore the flag on ba entry and use
    // these flags instead.
    // The implementation of these properties is that they will be set
    // after reading.
    var babyFlag = false
        get() = field.apply { field = true }
    var xabaFlag = false
        get() = field.apply { field = true }

    private fun fillHashTableAndQueue() : PriorityQueue
    {
        val maxCount = hashTable.fillHashTable()

        // Size the queue to be as big as needed or limit
        // to the square root of the number of symbols
        val queueSize = minOf(maxCount - 1, Math.sqrt(text.symbolCount.toDouble()).toInt())

        return PriorityQueue(queueSize).apply { fill() }
    }

    private tailrec fun HashTable.fillHashTable(index: Int = 0, maxCount: Int = 0): Int
        = if(index < text.symbolCount)
        {
            val nextIndex = text.nextIndex(index)
            val previousOccurrence = text.previousOccurrence(index)

            // Look only for the first occurrence of a pair that has at least
            // 2 non-overlapping occurrences. This test will find only those
            // indices that are the first one that has more than one non-
            // overlapping occurrences:
            // - If this is the only occurrence, previousOccurrence == index
            // - If this is not the first occurrence of the pair previous
            // occurrence < index
            // - If there are only 2 occurrences but they overlap as in aaa
            // which is 2 overlapping pairs then previous occurrence ==
            // next index
            // - If there are at least 2 occurrences of the pair that do not
            // overlap each other then previousOccurrence > nextIndex
            if (previousOccurrence > nextIndex)
            {
                val count = countNonOverlappingOccurrences(index)

                assert(count > 1)

                create(index, count).also { workingList.add(it) }

                fillHashTable(nextIndex, Math.max(maxCount, count))
            }
            else
            {
                fillHashTable(nextIndex, maxCount)
            }
        }
        else
        {
            maxCount
        }

    private fun PriorityQueue.fill() = workingList.forEach { addEntry(it) }

    private fun compress(dictionary: Dictionary): CharSequence
    {
        queue.forEach()
        {
            val index = it.index

            val firstSymbol = text.symbolAt(index)

            // Get the second symbol of the pair
            val secondSymbol = text.nextSymbol(index)

            // Save the definition of that new symbol into the dictionary
            val newSymbol = dictionary(firstSymbol, secondSymbol)

            log(REPAIR_INFO) {"starting replacement of '${firstSymbol.description}${secondSymbol.description}'" +
                    " with ${newSymbol.description} for $it "}

            hashTable.remove(it)

            // Replacement is done in two passes over the pairs as explained
            // in the paper.
            passOne(index, newSymbol, firstSymbol, secondSymbol)
            passTwo(index, newSymbol)
        }

        return text.collapse()
    }

    /**
     * This is the first of two passes to implement the replacement of the given
     * set of pairs with the newSymbol.
     *
     * To explain the operation of this algorithm the symbols xaby will be used
     * as in the paper. ab is the pair to be replaced and x and y are the
     * characters providing the context of the replacement.
     *
     * At the end of this pass the following will be true:
     *
     * The count of pairs that overlap the one to be replaced will be
     * decremented. The entries will still be in the hash table if they have a
     * count > 0. The entries will be removed from the queue and placed in a
     * working freeEntries which is emptied at the end of pass 2.
     *
     * If the count of a replaced entry gets down to 1 it will still be in the
     * hash table These entries cannot be deleted in this pass as we still need
     * them for storing a flag value. They will get deleted in pass 2.
     *
     * All occurrences of ab will be replaced by the new symbol A.
     *
     * New entries will be created and added to the hash table for any newly
     * created pairs (xA or Ay) that will have a count of at least 2. These
     * entries will not fully be initialized until pass 2. They will have a count
     * of zero and the index will <bold>not</bold> be pointing to the first
     * occurrence of the pair. Pass 2 will finish setting up these new pairs
     *
     * @param firstReplacement The first location to be replaced. This is the first in a
     * doubly linked circular freeEntries.
     * @param A The symbol the pair will be replaced with.
     * @param a The first symbol in the pair to be replaced.
     * @param b The second symbol in the pair to be replaced.
     */
    private fun passOne(firstReplacement: Int, A: Int, a: Int, b: Int)
    {
        AA_Flag.reset()
        babyFlag = false
        xabaFlag = false

        replacePairs(firstReplacement, A, a, b)
    }

    private tailrec fun replacePairs(replacement: Int, A: Int, a: Int, b: Int)
    {
        // Note there are three special cases that must be accounted
        // for in the replacement of xaby:
        // -- Consecutive pairs. This is when several pairs appear
        // back to back, such as when replacing ab in xabababy
        // which becomes xAAAy. This only creates one replaceable
        // AA pair.
        // -- Consecutive pairs of the same symbol. An example would
        // be replacing aa in xaaaay. That is 2 replacements not 3.
        // -- The occurrence of pair replacements like baby and xaba.

        // Get the indexes of x, a, b, and y
        val xIndex = text.previousIndex(replacement)
        val aIndex = replacement
        val bIndex = text.nextIndex(aIndex)
        val yIndex = text.nextIndex(bIndex)

        log(REPAIR_DEBUG) {"doing replacement for indices $xIndex $aIndex $bIndex $yIndex " +
                "'${text.describe(xIndex)}${text.describe(aIndex)}" +
                "${text.describe(bIndex)}${text.describe(yIndex)}'"}

        // Get the index of the next replacement, skipping any overlapping
        // pair
        val nextReplacement = text.nextNonOverlappingOccurrence(aIndex)

        // Flag to indicate that a new entry can be created for the new
        // left side pair (i.e. xA).
        val create_xA_Entry = xIndex >= 0 && when(text.symbolAt(xIndex))
        {
            // This checks for the special case of consecutive replacements as
            // in xabab. This check will evaluate to true after we replace
            // the first occurrence of ab and are about to replace the second,
            // in which case the context looks like Aaby.
            // Now we handle the special case of repeated new symbols as in
            // xababy going to xAAy. We need to determine if we need to
            // create an entry for it.
            A -> AA_Flag.isNotFirstAA(xIndex, aIndex)

            // When x is b as in baby then we have to use a separate flag
            b ->
            {
                removeIndex(xIndex)
                babyFlag
            }

            // We now check if this is the first or second
            // occurrence of the new pair. This is determined by
            // looking at the flag. On the first occurrence the
            // flag will be clear on the second occurrence it
            // will be set and we then create an entry for the
            // new pair.
            else -> removeIndex(xIndex)
        }

        // Flag to indicate that a new entry can be created for the new
        // right side pair (i.e. Ay). Normally this is true if this is
        // not the first time we have removed an Index for by.
        val create_Ay_Entry = removeIndex(bIndex).let()
        {
             text.isValidPairIndex(bIndex)
                && when (text.symbolAt(yIndex))
                {
                    // If y is the start of the next replacement, we can't create
                    // the AA pair yet because currently it is still Aa, not AA.
                    // We will handle it when this replacement is the x for
                    // that replacement
                    a -> nextReplacement != yIndex && xabaFlag
                    else -> it
                }
        }

        // Replace the pair with the new symbol. This must occur after
        // removing the indices for the overlapping pairs above because
        // changing the symbols would cause the pairs to not be found.
        text.replacePair(aIndex, A)

        // Create a new entry for the left side. This has to be after
        // the pair replacement so that the correct symbols are part of
        // the pair.
        if (create_xA_Entry)
        {
            log(REPAIR_VERBOSE) {"creating xA Entry"}

            getOrCreateEntry(xIndex)
        }

        // Create a new entry for the right side. This has to be after
        // the pair replacement so that the correct symbols are part of
        // the pair.
        if (create_Ay_Entry)
        {
            log(REPAIR_VERBOSE) {"creating Ay Entry"}

            getOrCreateEntry(aIndex)
        }

        if(nextReplacement > replacement)
        {
            replacePairs(nextReplacement, A, a, b)
        }
    }


    /**
     * Remove the given index from the set of pairs for that index. The steps
     * performed are:
     *
     *  * Look up the entry for the pair at the given index. If there is no
     * entry for the pair there is nothing to do except unlink the index from
     * other instances of the pair and false is returned.
     *  * Check to see if we have seen this pair before, which is indicated by
     * the flag on the entry being set. The value of this flag is what is
     * returned by this method.
     *  * If this is the first time to see this pair, remove the entry from the
     * priority queue and put it in the freeEntries of working entries, which sets its
     * flag
     *  * If the index was the first occurrence of the pair update the entry to
     * point to the next entry
     *  * Remove this index from the doubly-linked, circular freeEntries of other
     * pairs for the symbol, which determines if the count needs to change
     *  * Update the count of non-overlapping pairs. It is possible in the case
     * of a string of a repeated symbols that the count does not decrement.
     *  * Remove the item from the hash table if there are no remaining
     * non-overlapping occurrences of the pair
     *
     * @param indexToRemove The index to be removed
     * @return true If this is not the first time we have seen this pair.
     */
    internal fun removeIndex(indexToRemove: Int)
        = if(text.isValidPairIndex(indexToRemove))
        {
            val entry = hashTable.findPair(indexToRemove)
            val seenBefore = entry?.flag ?: false

            log(REPAIR_VERBOSE) {"removing index $indexToRemove for $entry, seen before = $seenBefore"}

            if (entry != null)
            {
                // If this is the first time we have seen this pair move it
                // to the working entries freeEntries.
                if (!seenBefore)
                {
                    // Remove the entry from the priority queue, which will set the flag for the entry
                    queue.removeEntry(entry)
                    workingList.add(entry)
                }

                // If this is the index the entry is pointing to we need
                // to point the entry to the next occurrence
                if (entry.index == indexToRemove)
                {
                    // Change the entry to point to the next occurrence.
                    entry.index = text.nextOccurrence(indexToRemove)
                }

                // Unlink this occurrence from other occurrences,
                // which returns how much to change the count by
                entry.count += text.recountOverlappingPairs(indexToRemove)


                // If there are no more occurrences, we have to get rid
                // of the entry
                if (entry.count < 1)
                {
                    entry.log(REPAIR_DEBUG) {"discarding $this since there are no more occurrences"}
                    workingList.remove(entry)
                    hashTable.remove(entry)
                }
            }

            // Unlink this occurrence from other occurrences
            text.unlink(indexToRemove)

            seenBefore
        }
        else
        {
            false
        }

    internal fun getOrCreateEntry(indexToCreate: Int)
        = hashTable.findPair(indexToCreate)?.log(REPAIR_VERBOSE) {"found $this for index $indexToCreate"}
            ?: hashTable.create(indexToCreate, 0).also{ workingList.add(it) }
            .log(REPAIR_VERBOSE) {"got or reclaimed $this for index $indexToCreate"}

    /**
     * After the first pass most of the work is done, but there are a couple of
     * loose ends to tie up, which is what this second pass takes care of.
     *
     * <ul>
     *   <li>The new replacement symbols are still linked together.</li>
     *   <li>There can be some entries for pairs that previously overlapped a
     * replacement that are down to a count of 1, but are still in the hash
     * table</li>
     *   <li>Any entry that was modified or created during pass one is not in the
     * queue.</li>
     *  <li>Entries for any new pairs involving the replacement (xA and Ay) have
     * been created, but their count is still zero and the links for the pairs
     * have not been made. The index in the entry will actually be pointing at
     * the second occurrence of the pair instead of the first</li>
     *
     * @param nextIndex
     * @param A
     */
    private fun passTwo(nextIndex: Int, A: Int)
    {
        tailrec fun unlinkAndAddNewPairs(i: Int)
        {
            // Save the index for the next occurrence before we unlink it
            val nextOccurrence = text.nextOccurrence(i)

            // The index of the character to the left of replacement
            val xIndex = text.previousIndex(i)
            val yIndex = text.nextIndex(i)

            // Remove this occurrence from the list
            text.unlink(i)

            // For consecutive replacements we will handle
            // those on the right side
            if (text.isValidPairIndex(xIndex) && text.symbolAt(xIndex) != A)
            {
                // Add this xA instance
                addIndex(xIndex, 1)
            }

            if(text.isValidIndex(yIndex))
            {
                // The amount by which the count for Ay changes
                // which is usually 1, but may be zero in the
                // case of a string of AAA
                val countDeltaForAy = when (text.symbolAt(yIndex))
                {
                    A -> if (AA_Flag.isNonOverlappingAA(xIndex, i)) 1 else 0
                    else -> 1
                }

                // Add this Ay instance
                addIndex(i, countDeltaForAy)
            }

            if(i != nextOccurrence)
            {
                unlinkAndAddNewPairs(nextOccurrence)
            }
        }

        AA_Flag.reset()

        unlinkAndAddNewPairs(nextIndex)

        // All the entries that were decremented or created are
        // in a working freeEntries and not in the queue. We need to
        // put them all into the queue, which clears their flags,
        // and discard them if they only have one occurrence left
        workingList.forEach()
        {
            // Put the entry in the queue, which will clear its flag
            // If there are too few occurrences remaining,
            // to add to the queue, remove it from the hashTable
            if(!queue.addEntry(it))
            {
                it.log(REPAIR_DEBUG) {"discarding $this since there are < 2 occurrences"}
                hashTable.remove(it)
            }
        }
    }

    internal fun addIndex(indexToAdd: Int, countIncrement: Int)
    {
        hashTable.findPair(indexToAdd)?.apply()
        {
            log(REPAIR_VERBOSE) {"Adding $indexToAdd to $this"}

            when(count)
            {
                // If the count is set to zero this means this is the first
                // occurrence. Set the index to this location
                0 -> index = indexToAdd

                // This is not the first occurrence, so add it to the
                // end of the freeEntries
                else -> text.linkBefore(indexToAdd, index)
            }

            count += countIncrement
        }
    }
}
