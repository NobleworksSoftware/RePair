package com.nobleworks_software.repair

import com.nobleworks_software.repair.PairBuffer.Companion.hashPair
import com.nobleworks_software.repair.util.OpenAddressHashTable

internal class HashTable(private val text: PairBuffer)
    : OpenAddressHashTable<PriorityQueueEntry>(text.symbolCount)
{
    override val PriorityQueueEntry.hash get() = text.hash(index)

    private fun PriorityQueueEntry.equals(first: Int, second: Int) = text.equals(index, first, second)

    /**
     * Find the PriorityQueueEntry record for the pair of symbols at
     * the given index. If there is no entry for that pair, null is returned.
     *
     * @param index The index for the pair.
     * @return The PriorityQueueEntry for the pair or null if none is found.
     */
    fun findPair(index: Int): PriorityQueueEntry?
    {
        val first = text.symbolAt(index)
        val second = text.nextSymbol(index)

        return findValue(hashPair(first, second)) { it.equals(first, second) }
                .log(HASHTABLE_DEBUG) {"search for '${first.description}${second.description}' returns $this"}
    }

    private var freeEntries: PriorityQueueEntry? = null

    fun create(index: Int, count: Int): PriorityQueueEntry
        = (freeEntries
                ?.also { it.init(index, count) }
                ?.also { freeEntries = it.removeFrom(freeEntries) }
                ?: PriorityQueueEntry(index, count))
                .also { insert(it) }

    override fun remove(value: PriorityQueueEntry)
    {
        super.remove(value.log(HASHTABLE_DEBUG) {"Removing $value from hash table"}
        )

        freeEntries = value.addBefore(freeEntries)
    }

    override fun insert(value: PriorityQueueEntry)
    {
        super.insert(value.log(HASHTABLE_DEBUG) {"Inserting $value into hash table"})
    }
}