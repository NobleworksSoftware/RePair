@file:Suppress("RedundantVisibilityModifier")

package com.nobleworks_software.repair

/**
 * Interface to a dictionary for RePair. The job of the dictionary is to control
 * the allocation of replacement code points for pairs being replaced.
 *
 * Add the given pair of Unicode code points to the dictionary, assigning a
 * new code point as the replacement. The replacement code point should not
 * appear in the original input. An easy choice is to use Private Use area
 * on supplemental planes 15 and/or 16.
 * @author Dale King, Nobleworks Software
 */
public typealias Dictionary = (Int, Int) -> Int
