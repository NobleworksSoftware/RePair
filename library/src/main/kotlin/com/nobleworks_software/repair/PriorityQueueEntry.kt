package com.nobleworks_software.repair

import com.nobleworks_software.repair.util.DoublyLinkedListItem

/**
 * This is the class for an actual entry in the priority queue.
 * Entries can be arranged in a doubly-linked list.
 *
 * @param index The index of the first occurrence of the pair.
 * @param count The number of non-overlapping occurrences.
 */
@Suppress("RemoveEmptyParenthesesFromLambdaCall")
class PriorityQueueEntry(index: Int, count: Int) : DoublyLinkedListItem<PriorityQueueEntry>()
{
    var index = index
        set(value)
        {
            field = value.log(ENTRY_VERBOSE) {"${this@PriorityQueueEntry} setting index to $value"}
        }
    companion object
    {
        private const val FLAG_MASK = 0x80000000.toInt()
    }

    /**
     * This is the number of non-overlapping occurrences of the pair in the
     * `PairBuffer`. Note that this may be less than the number of
     * occurrences in the list as some pairs may overlap.
     * <P>
     * The most significant bit is used as a special flag that indicates if the
     * entry is currently in the priority queue.
     */
    private var countAndFlag: Int = count or FLAG_MASK

    fun init(index:Int, count: Int)
    {
        this.countAndFlag = count or FLAG_MASK
        this.index = index
    }

    /**
     * The number of non-overlapping occurrences of the pair.
     */
    var count: Int
        get() = countAndFlag and FLAG_MASK.inv()
        set(value)
        {
            countAndFlag = (countAndFlag and FLAG_MASK) or
                    value.log(ENTRY_VERBOSE) {"${this@PriorityQueueEntry} setting count to $value"}
        }

    /**
     * The value of a special flag that is associated with an entry for
     * book-keeping purposes.
     */
    var flag: Boolean
        get() = (countAndFlag and FLAG_MASK) != 0
        set(value) { countAndFlag = if (value) count or FLAG_MASK else count}

    /**
     * Compare this entry to the other entry based on <B>descending</B> counts
     */
    override fun compareTo(other: PriorityQueueEntry) = Integer.compare(other.count, count)

    override val self: PriorityQueueEntry get() = this

    override fun toString(): String = "Entry[$index, $count]"
}
