package com.nobleworks_software.repair

import java.nio.CharBuffer

/**
 * This is a special data type used by the Re-Pair algorithm. It holds the text
 * as it undergoes replacement of pairs of symbols by a new dictionary symbol.
 *
 * The data is held in an array. There are also arrays of links to the next and
 * previous occurrence of a pair in the text.
 *
 * This implementation has some slight variations from the one in the paper.
 * Here are a list of variations and clarifications.
 *
 *  * There is no special skip symbol instead the presence of a low surrogate
 * acts the same as a skip symbol. When a pair is replaced with a new symbol two
 * chars are stored in the buffer. If the replacement is in a supplementary
 * plane the second char is automatically a a low surrogate. If the replacement
 * is in the BMP then a low surrogate is added.
 *  * There is another special symbol called the `separatorSymbol`.
 * This special symbol can act as a separator between pieces of text in the pair
 * buffer. It will never become part of a pair.
 *  * Pairs are maintained in a doubly-linked *circular* list. The paper
 * only uses a doubly-linked list, but I found it more efficient to use a
 * circular list, because we need to insert entries at the tail of the list.
 *
 * To use it you provide a `CharSequence` that provides access to the
 * text to be added.
 *
 * @author Dale King
 * @param input The text to put in the buffer
 */
@Suppress("RemoveEmptyParenthesesFromLambdaCall")
internal class PairBuffer(input: CharSequence)
{
    /**
     * This is the array containing the text with a separator before and after the text
     */
    private val text: CharArray = CharArray(input.length) { input[it] }

    /**
     * This is the array containing the index for the next occurrence of the
     * pair within the text in a circular list. If there are no more occurrences
     * in the text the value will point to the first occurrence.
     *
     * If the symbol at this position is a low surrogate the index in this array
     * is the index for the next symbol.
     */
    // Initially start with each entry pointing to itself
    private var next = IntArray(text.size) { it }

    /**
     * The number of unicode code points in the text, which can be less than the
     * number of UTF-16 characters that were passed to the constructor.
     */
    val symbolCount = processSymbols()
            .log(PAIRBUFFER_INFO) {"input had $this code points"}

    /**
     * This is the array containing the index for the previous occurrence of the
     * pair within the text in a circular list. If there are no previous
     * occurrences in the text the value will point to the last occurrence.
     *
     * If the symbol at this position is a low surrogate the index in this array
     * is the index for the start of the code point before this index
     */
    private var prev = buildLists()

    /**
     * Process the input to check for code points that are encoded as more than one
     * character. The next pointer for the low surrogate in a surrogate pair is
     * changed to point to the next code point after the pair.
     * @return The number of Unicode code points in the input not counting the
     * separator characters added before and after the text
     */
    private tailrec fun processSymbols(i: Int = 0, count: Int = 0) : Int
    {
        if(i < next.size)
        {
            val symbol = Character.codePointAt(text, i)

            val charCount = Character.charCount(symbol)

            if (charCount > 1)
            {
                // For surrogate pairs the next pointer of the low surrogate
                // points to the next symbol
                next[i + 1] = i + charCount
            }
            // Check for unpaired surrogates which we cannot handle
            else if (Character.getType(symbol) == Character.SURROGATE.toInt())
            {
                throw IllegalArgumentException("RePair input contains an unmatched surrogate at offset $i")
            }

            return processSymbols(i + charCount, count + 1)
        }
        else
        {
            return count
        }
    }

    // Search in the hash table for previous occurrences of this
    // pair or an empty slot where the index of the pair should
    // be inserted.
    private tailrec fun IntArray.find(first: Int, second: Int, hash: Int = hashPair(first, second)): Int
    {
        // Compute the hash value for the symbol pair
        val hashIndex = hash % size
        val value = this[hashIndex]

        return if(value < 0 || equals(value, first, second))
        {
            hashIndex
        }
        else
        {
            find(first, second, hash + 1)
        }
    }

    private tailrec fun IntArray.buildNextLists(i: Int = 0, first: Int = symbolAt(i))
    {
        if(i < text.size - 1)
        {
            val nextI = nextIndex(i)
            val second = symbolAt(nextI)

            // Make sure neither is the separator symbol, since it cannot be
            // part of a pair.
            if (first != RePair.separatorSymbol && second != RePair.separatorSymbol)
            {
                val hashIndex = find(first, second)
                val previousIndex = this[hashIndex]
                this[hashIndex] = i

                if (previousIndex >= 0)
                {
                    // This is the new end of the list point it to
                    // the start of the list.
                    next[i] = next[previousIndex]
                    // Point the previous occurrence to this one.
                    next[previousIndex] = i
                }
            }

            buildNextLists(nextI, second)
        }
    }

    /**
     * Create the doubly-linked circular lists of symbol pairs. (Note that this
     * does create overlapping pairs).
     */
    private fun buildLists() : IntArray
        = IntArray(text.size){ -1 }.apply()
        {
            buildNextLists()

            // Convert the array we used as a temporary hash table into the prev array
            indices.forEach()
            { i ->
                if (Character.isLowSurrogate(text[i]))
                {
                    this[i] = i - 1
                }
                else
                {
                    // Link that next occurrence back to this one.
                    this[next[i]] = i
                }
            }
        }

    /**
     * Determines if the pair of symbols at the given index is equal to the pair
     * of symbols given.
     *
     * @param index The index in question.
     * @param first The desired first symbol.
     * @param second The desired second symbol.
     */
    fun equals(index: Int, first: Int, second: Int) = first == symbolAt(index) && second == nextSymbol(index)

    /**
     * Get the symbol at the given index.
     *
     * @return The code point at that index.
     * @param index Index for the symbol to return.
     */
    internal fun symbolAt(index: Int): Int = Character.codePointAt(text, index)

    /**
     * Get the next symbol after the given index.
     *
     * @return The symbol after the given index.
     * @param index Index prior to the symbol to return.
     */
    fun nextSymbol(index: Int) = symbolAt(nextIndex(index))

    /**
     * Get the index for the next symbol after the given index.
     *
     * @return The index of the next symbol.
     * @param index Index prior to the index to return.
     */
    fun nextIndex(index: Int): Int
        = (index + 1).takeUnless { it < text.size && Character.isLowSurrogate(text[it]) }
            ?: next[index + 1]

    /**
     * Get the index of the next occurrence of the pair at the given index.
     *
     * @return The index of the next occurrence of the pair. If there are no
     *         more occurrences, the index will be returned
     * @param index Index of the pair to find the next occurrence of.
     */
    fun nextOccurrence(index: Int) = next[index]

    fun nextNonOverlappingOccurrence(index: Int): Int
    {
        val nextIndex = nextIndex(index)
        val nextOccurrence = next[index]

        return  if (nextOccurrence == nextIndex) next[nextOccurrence] else nextOccurrence
    }

    /**
     * Get the index of the previous occurrence of the pair at the given index.
     *
     * @return The index of the previous occurrence of the pair. If there are no
     *         more occurrences, the index will be returned. If the symbol at
     *         the given index is the `skipSymbol` then the value
     *         returned is the index for the previous symbol.
     * @param index Index of the pair to find the previous occurrence of.
     */
    fun previousOccurrence(index: Int) = prev[index]

    /**
     * Get the index for the symbol before the given index.
     *
     * @return The index of the previous symbol.
     * @param index Index after the index to return.
     */
    fun previousIndex(index: Int): Int
        = (index - 1).takeUnless { it >= 0 && Character.isLowSurrogate(text[it])} ?: prev[index - 1]

    /**
     * Replace the pair at the given index, with the given new symbol.
     *
     * @param index Location of the first symbol of the pair.
     * @param newSymbol The symbol used to replace the pair.
     */
    internal fun replacePair(index: Int, newSymbol: Int)
    {
        val replacementChars = charArrayOf('\uD800', '\uDC00')

        Character.toChars(newSymbol, replacementChars, 0)

        // Get the index of the second symbol in the pair
        val secondIndex = nextIndex(index)

        // Get the index of the symbol following the pair
        val rightIndex = nextIndex(secondIndex)

        // Set the symbol next to the symbol following the pair to
        // point to the new symbol. This is to maintain the backward
        // links over a block of skipped symbols.
        prev[rightIndex - 1] = index

        // Set the symbol following the new symbol to point to the
        // symbol following the pair. This is to maintain the forward
        // links over a block of skipped symbols.
        next[index + 1] = rightIndex

        // Replace the first symbol of the pair with the new symbol
        text[index] = replacementChars[0]

        // Replace the character after the first symbol to complete surrogate pair
        text[index + 1] = replacementChars[1]

        // Replace the second symbol in the pair with surrogate as well
        text[secondIndex] = replacementChars[1]
    }

    tailrec fun determineCountDeltaForward(index: Int, delta: Int) : Int
    {
        // Scan the following overlapping pairs toggling between a delta
        // of -1 and 0
        val n = next[index]

        return if(nextIndex(index) == n) determineCountDeltaForward(n, -1 - delta) else delta
    }

    // This logic determines whether the removal of the pair
    // should cause the count to decrement. This will be true
    // if there are no pairs that overlap this one. It may or
    // may not be true in the case for overlapping pairs, which
    // occurs with runs of the same symbol. If for instance you
    // had the text aaaaa which is 4 pairs of aa, but only 2 of
    // the pairs are non-overlapping. The removal of the first
    // or last pair from the lie does not constitute a
    // decrement in the count of aa pairs as two non-overlapping
    // pairs remain. However for aaaa it does because only one
    // non-overlapping pair will remain. It basically boils down
    // to whether the number of overlapping pairs is odd or even.
    tailrec fun determineCountDelta(index: Int, delta: Int = -1) : Int
    {
        // Scan the preceding overlapping pairs toggling between a delta
        // of -1 and 0
        val p = prev[index]

        return when
        {
            previousIndex(index) == p -> determineCountDelta(p, -1 - delta)
            delta != 0 -> determineCountDeltaForward(index, delta)
            else -> 0
        }
    }

    /**
     * Determine how much removing the pair at the given index will change the number
     * of non-overlapping pairs.
     *
     * @param index The index to be checked.
     * @return the amount that the number of overlapping pairs changes which is 0 or -1
     */
    fun recountOverlappingPairs(index: Int) = determineCountDelta(index)

    /**
     * Remove the links for the symbol at the given index.
     *
     * @param index The index to be unlinked.
     */
    fun unlink(index: Int)
    {
        // Get the indexes of the previous and next occurrence to this one
        val prevIndex = prev[index]
        val nextIndex = next[index]

        // Change previous one to point to the next occurrence after this one
        next[prevIndex] = nextIndex

        // Change next one to point to the previous occurrence before this one
        prev[nextIndex] = prevIndex

        // Remove the links for this occurrence.
        prev[index] = index
        next[index] = index
    }

    /**
     * Insert the pair at `index` into the doubly linked list so that
     * it precedes the occurrence at `nextIndex`.
     *
     * @param index Index to be inserted into the list.
     * @param nextIndex Index of the node that will come after the new one in the list.
     */
    fun linkBefore(index: Int, nextIndex: Int)
    {
        // Get the index for the occurrence that will follow the new one.
        val prevIndex = prev[nextIndex]

        // Set the links at this index
        prev[index] = prevIndex
        next[index] = nextIndex

        // Set the previous occurrence to point to this one.
        next[prevIndex] = index

        // Point it to this new one
        prev[nextIndex] = index
    }

    fun collapse(): CharSequence
    {
        tailrec fun collapse(source: Int, dest: Int) : CharSequence
            = if(source < text.size)
            {
                val len = Character.toChars(symbolAt(source), text, dest)
                collapse(nextIndex(source), dest + len)
            }
            else
            {
                CharBuffer.wrap(text, 0, dest)
            }

        return collapse(0, 0)
    }

    fun hash(index: Int) = hashPair(symbolAt(index), nextSymbol(index))

    fun isValidPairIndex(index: Int) = index >= 0 && index < text.size - 1

    fun isValidIndex(index: Int) = index >= 0 && index < text.size

    companion object
    {
        /**
         * Computes a hash value for the given pair
         * of symbols.
         *
         * @param a The first symbol of the pair.
         * @param b The second symbol of the pair.
         * @return A hash value computed from the combination of the two symbols.
         */
        fun hashPair(a: Int, b: Int): Int
        {
            return Math.abs(a * (a + b + 1) + b * (b + 1))
        }
    }

    fun describe(index: Int) : String
        = when
        {
            index < 0 -> "<BOF>"
            index > text.lastIndex -> "<EOF>"
            else -> symbolAt(index).description
        }
}
