package com.nobleworks_software.repair

const val HASHTABLE_INFO       = 0b000000000000000001
const val HASHTABLE_DEBUG      = 0b000000000000000011
const val HASHTABLE_VERBOSE    = 0b000000000000000111

const val PAIRBUFFER_INFO      = 0b000000000000001000
const val PAIRBUFFER_DEBUG     = 0b000000000000011000
const val PAIRBUFFER_VERBOSE   = 0b000000000000111000

const val QUEUE_INFO           = 0b000000000001000000
const val QUEUE_DEBUG          = 0b000000000011000000
const val QUEUE_VERBOSE        = 0b000000000111000000

const val ENTRY_INFO           = 0b000000001000000000
const val ENTRY_DEBUG          = 0b000000011000000000
const val ENTRY_VERBOSE        = 0b000000111000000000

const val REPAIR_INFO          = 0b000001000000000000
const val REPAIR_DEBUG         = 0b000011000000000000
const val REPAIR_VERBOSE       = 0b000111000000000000

const val WORKING_LIST_INFO    = 0b001000000000000000
const val WORKING_LIST_DEBUG   = 0b011000000000000000
const val WORKING_LIST_VERBOSE = 0b111000000000000000

const val HASHTABLE_OFF        = 0
const val PAIRBUFFER_OFF       = 0
const val QUEUE_OFF            = 0
const val ENTRY_OFF            = 0
const val REPAIR_OFF           = 0
const val WORKING_LIST_OFF     = 0

const val LOG_LEVEL =
        HASHTABLE_OFF or
        PAIRBUFFER_OFF or
        QUEUE_OFF or
        ENTRY_OFF or
        REPAIR_OFF or
        WORKING_LIST_OFF

inline fun log(logLevel: Int, message: () -> String)
{
    if(LOG_LEVEL and logLevel != 0) println(message())
}

inline fun <T> T.log(logLevel: Int, message: T.() -> String) : T
{
    if(LOG_LEVEL and logLevel != 0) println(message())
    
    return this
}

val Int.description get() =
        when(Character.getType(this).toByte())
        {
            Character.PRIVATE_USE -> "[${(this and 0xFFF).toString(16)}]"
            else -> "${this.toChar()}"
        }

